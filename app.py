from flask import Flask
from cryptography.fernet import Fernet

app = Flask(__name__)

key = Fernet.generate_key()
fernet = Fernet(key)


@app.route("/encrypt")
def encrypt():
    string = request.args.get("string")

    encrypted_string = fernet_key.encrypt(bytes(string, "utf-8"))
    return f"Encrypted string: {encrypted_string}"


@app.route("/decrypt")
def decrypt():
    string = request.args.get("string")

    decrypted_string = fernet_key.decrypt(bytes(string, 'utf-8'))
    return f"Decrypted string: {decrypted_string}"


if __name__ == "__main__":
    app.run()
