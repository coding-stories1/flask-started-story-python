# Flask Started Story


## Story Outline

How create simple web application by Flask microframework? How organize your code 
or which additional config files I should add to check quality of code? 

Answer for all these things you can find in this small story

- We create simple Flask application
- Add `Poetry` to control packages in the project
- Organize code in folders


## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #flask #python #started
